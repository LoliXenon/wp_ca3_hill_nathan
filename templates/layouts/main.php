<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?= APP_BASE_PATH ?>/assets/styles/reset.css">
    <link rel="stylesheet" href="<?= APP_BASE_PATH ?>/assets/styles/site.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src='<?= APP_BASE_PATH ?>/assets/scripts/site.js'></script>
    <title><?= $locals['pageTitle'] ?? 'Default Title' ?></title>
</head>

<body>

<header>
<h1>Website</h1>
    <nav>
        <ul>
        <li><a class='btn' href="<?= APP_BASE_PATH ?>/">Home</a></li>
        <li><a class='btn' href="<?= APP_BASE_PATH ?>/ship_list">Ship List</a></li>
        <li><a class='btn' href="<?= APP_BASE_PATH ?>/navy_list">Navy List</a></li>
        </ul>
    </nav>
</header>

<main>

<?= \Rapid\Renderer::VIEW_PLACEHOLDER ?>

</main>
</body>
</html>
