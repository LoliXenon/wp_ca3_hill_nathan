<h2>Remove ship from the Database</h2>

<form action='<?= APP_BASE_PATH ?>/remove_ship' method='post'>

<label for='ship_ID' id='ship_ID'>Select ship to remove</label>

<select name="ship_ID" id="ship_ID">
 <?php foreach ($locals['array'] as $submission) { ?>
 <option value="<?= $submission['ship_ID'] ?>"><?= $submission['ship_name'] ?></option>
 <?php } ?>
 </select>
 
<input type="submit" value="Remove Ship">
</form>