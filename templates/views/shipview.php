<p>
<h2>List of ships in the Database</h2>

<?php if ($locals['success'] === TRUE) { ?>
<p>Successfully updated the ship</p>
<?php } ?>

<ul>
 <?php foreach ($locals['array'] as $submission) { ?>
<li>
<?= $submission['navy_acronym'] ?> <?= $submission['ship_name'] ?>: <?= $submission['ship_class'] ?>. 
<a href="<?= APP_BASE_PATH ?>/update_ship?ship_ID=<?= $submission['ship_ID'] ?>">Update Ship</a>
</li>
<?php }?>
</ul>
</p>

<div>

<a class='btn' href="<?= APP_BASE_PATH ?>/add_ship">Add Ship</a>
<a href="<?= APP_BASE_PATH ?>/remove_ship" class="btn">Remove Ships</a>

</div>