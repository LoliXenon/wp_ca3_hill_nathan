<p>
<h2>
Update Navy Entry
<?= $locals['navy_ID'] ?>
</h2>
<form action="<?= APP_BASE_PATH ?>/update_navy?navy_ID=<?= $locals['array']['navy_ID'] ?>" method="post">

<div>
<label for="navy_name">navy Name</label>
<input type="text" name="navy_name" pattern="^([A-Za-z0-9\s]{1,255})$" title="No special characters" id="navy_name" value="<?= $locals['array']['navy_name'] ?>">
</div>

<div>
<label for="navy_acronym">Navy Acronym</label>
<input type="text" name="navy_acronym" pattern="^[A-Za-z]{3}$" title="Three letters" id="navy_acronym" value="<?= $locals['array']['navy_acronym'] ?>" >
</div>

<div>
<input type="submit" value="Update Navy">
</div>

</form>

</p>