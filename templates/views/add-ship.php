<h2>Add a new ship</h2>
<form action='<?= APP_BASE_PATH ?>/add_ship' method='post'>

<?php if ($locals['success'] === TRUE) { ?>
<p>Successfully added the ship to the database</p>
<?php } ?>

<div>
<label for="navy_ID" id="navy_ID">Navy</label>
<select name="navy_ID" id="navy_ID">
    <?php foreach ($locals['array'] as $submission) { ?>
        <!-- 
                Adjusted by Cameron Scholes
                Your original code  I had to take out the php ? and = to avoid errors
        <option value="$submission['navy_id']">$submission['acronym']</option>
        -->
    <option value="<?= $submission['navy_ID'] ?>"><?= $submission['navy_acronym'] ?></option>
    <?php } ?>
</select>
</div>

<div>
<label for="type">Ship Class</label>
<input type="text" name="ship_class" pattern="^([A-Za-z0-9\s]{1,255})$" title="No special characters" id="ship_class">
</div>

<div>
<label for="name">Ship Name</label>
<input type="text" name="ship_name" pattern="^([A-Za-z0-9zs]{1,255})$" title="No special characters" id="ship_name">
</div>

<div>
<input type="submit" value="Add Ship">
</div>

</form>