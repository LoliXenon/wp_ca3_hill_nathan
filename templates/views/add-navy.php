<h2>Add a new navy</h2>

<form action='<?= APP_BASE_PATH ?>/add_navy' method='post'>

<div>
<label for="navy_name">Navy Name</label>
<input type="text" name="navy_name" pattern="^([A-Za-z0-9\s]{1,255})$" title="No special characters" id="navy_name">
</div>

<div>
<label for="navy_acronym">Navy's Ship Acronym</label>
<input type="text" name="navy_acronym" pattern="^[A-Za-z]{3}$" title="Three letters" id="navy_acronym">
</div>

<div>
<input type="submit" value="Add Navy">
</div>

</form>
