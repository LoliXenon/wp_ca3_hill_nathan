<p>
<h2>
Update Ship Entry
</h2>
<form action="<?= APP_BASE_PATH ?>/update_ship?ship_ID=<?= $locals['array']['ship_ID'] ?>" method="post">

<div>
<label for="ship_name">Ship Name</label>
<input type="text" name="ship_name" pattern="^([A-Za-z0-9\s]{1,255})$" title="No special characters" id="ship_name" value="<?= $locals['array']['ship_name'] ?>">
</div>

<div>
<label for="ship_type">Ship Type</label>
<input type="text" name="ship_class" pattern="^([A-Za-z0-9\s]{1,255})$" title="No special characters" id="ship_class" value="<?= $locals['array']['ship_class'] ?>">
</div>

<div>
<input type="submit" value="Update Ship">
</div>

</form>

</p>