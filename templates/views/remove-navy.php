<h2>Remove navy from the Database. Make sure it has no ships in the database.</h2>

<form action='<?= APP_BASE_PATH ?>/remove_navy' method='post'>

<label for='navy_ID' id='navy_ID'>Select navy to remove</label>

<select name="navy_ID" id="navy_ID">
 <?php foreach ($locals['array'] as $submission) { ?>
 <option value="<?= $submission['navy_ID'] ?>"><?= $submission['navy_name'] ?></option>
 <?php } ?>
 </select>
 
<input class="btn external" type="submit" value="Remove Navy">
</form>