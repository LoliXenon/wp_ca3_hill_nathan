<p>
<h2>List of navies in the Database</h2>

<?php if ($locals['success'] === TRUE) { ?>
<p>Successfully updated the navy</p>
<?php } ?>

<ul> <?php foreach ($locals['array'] as $submission) { ?>
<li><?= $submission['navy_acronym'] ?>: <?= $submission['navy_name'] ?>. 
<a href="<?= APP_BASE_PATH ?>/update_navy?navy_ID=<?= $submission['navy_ID'] ?>">Update Navy</a></li>
<?php }?>
</ul>
</p>

<div>
<a class='btn' href="<?= APP_BASE_PATH ?>/add_navy">Add Navy</a>

<a href="<?= APP_BASE_PATH ?>/remove_navy" class="btn">Remove Navy</a>
</div>