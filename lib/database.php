<?php

    $config = include('config.php');

/**
 *              Adjusted by Cameron Scholes
 *              Your original code
 *
 * $db = new PDO(
'mysql:host=localhost;dbname=wp_ca3_Hill_Nathan;charset=utf8mb4',
'root',
'');
 */
    //Changed PDO creation to use config file
$db = new PDO(
    'mysql:host=' . $config['host'] . ';dbname=' . $config['name'] . ';charset=utf8mb4',
    $config['username'],
    $config['password']);

    return $db;

?>