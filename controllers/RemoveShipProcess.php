<?php return function($req, $res)
{

    $shipID = $req->body('ship_ID');


    try
    {
        $db = include_once('lib/database.php');

        $prepared = "DELETE FROM ships WHERE ship_ID = :ship_ID";
        $shipRemove = $db->prepare($prepared);
        $shipRemove->bindParam("ship_ID", $shipID);
        $shipRemove->execute();
        $shipRemove->closeCursor();

        $res->redirect('/remove_ship?success=1');
    }
    catch (PDOException $e)
    {
        $res->redirect('/remove_ship?success=0');
    }
    catch (Exception $e)
    {
        echo("balls");
    }

}


?>