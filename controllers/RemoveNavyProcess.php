<?php return function($req, $res)
{

    $navyID = $req->body('navy_ID');


    try
    {
        $db = include_once('lib/database.php');

        $prepared = "DELETE FROM navy WHERE navy_ID = :navy_ID";
        $navyRemove = $db->prepare($prepared);
        $navyRemove->bindParam("navy_ID", $navyID);
        $navyRemove->execute();
        $navyRemove->closeCursor();

        $res->redirect('/remove_navy?success=1');
    }
    catch (PDOException $e)
    {
        $res->redirect('/remove_navy?success=0');
    }
    catch (Exception $e)
    {
        echo("balls");
    }

}


?>