<?php 
return function($req, $res)
{

$navyName = $req->body('navy_name');
$navyAcronym = $req->body('navy_acronym');

try
{
    $db = include_once('lib/database.php');

    $prepared = "INSERT INTO navy (navy_name, navy_acronym) VALUES (:navy_name, :navy_acronym)";
    $navyEntry = $db->prepare($prepared);
    $navyEntry->bindParam(":navy_name", $navyName, PDO::PARAM_STR);
    $navyEntry->bindParam(":navy_acronym", $navyAcronym, PDO::PARAM_STR);
    $navyEntry->execute();

    $res->redirect('/add_navy?success=1');
}
catch (PDOException $e)
{
    $res->redirect('/add_navy?success=0');
}
catch (Exception $e)
{
    echo("fuck");
}

}
?>