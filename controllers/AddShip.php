<?php return function($req, $res)
{

    $db = include_once('lib/database.php');

    $shipList = $db->prepare("SELECT navy_ID, navy_acronym 
    FROM navy");
    $shipList->execute();
    $result = $shipList-> fetchAll();

    $res->render('main', 'add-ship', [
        'array' => $result,
        'pageTitle' => 'Add Ship',
        'success' => $req->query('success') === '1'
    ]);
}

?>