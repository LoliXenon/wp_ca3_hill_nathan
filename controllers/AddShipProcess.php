<?php return function($req, $res)
{
    $shipNavy = $req->body('navy_ID');
    $shipName = $req->body('ship_name');
    $shipClass = $req->body('ship_class');

    

    try
    {
        $db = include_once('lib/database.php');

        $prepared = "INSERT INTO ships (navy_ID, ship_name, ship_class) VALUES (:navy_ID, :ship_name, :ship_class)";
        $shipEntry = $db->prepare($prepared);
        $shipEntry->bindParam(":navy_ID", $shipNavy, PDO::PARAM_STR);
        $shipEntry->bindParam(":ship_name", $shipName, PDO::PARAM_STR);
        $shipEntry->bindParam(":ship_class", $shipClass, PDO::PARAM_STR);
        $shipEntry->execute();

        $res->redirect('/add_ship?success=1');
    }
    catch (PDOException $e)
    {
        $res->redirect('/add_ship?success=0');
    }
    catch (Exception $e)
    {
        echo("Fuck");
    }

    // $res->redirect('/add_ship?success=1');
}


?>