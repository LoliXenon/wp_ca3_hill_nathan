<?php

return function($req, $res)
{
    $db = include_once('lib/database.php');

    $prepared = "SELECT ship_name, ship_ID FROM ships";
    $shipList = $db->prepare($prepared);
    $shipList->execute();
    $result = $shipList->fetchAll();

    $res->render('main', 'remove-ship', [
        'array' => $result
    ]);
}


?>