<?php

return function($req, $res)
{

    $db = include_once('lib/database.php');

    $shipList = $db->prepare("SELECT s.ship_ID, s.navy_ID, s.ship_name, s.ship_class, n.navy_acronym
    FROM ships s, navy n
    WHERE s.navy_id = n.navy_id
    ORDER BY s.navy_ID");
    $shipList->execute();
    $result = $shipList-> fetchAll();

    $res->render('main', 'shipview', [
        'array' => $result,
        'pageTitle' => 'Ship List'
    ]);
};

// $shipList = $db->query("SELECT ships.ship_ID, ships.navy_ID, ships.ship_name, ships.ship_class, navy.acronym FROM ships JOIN navy ON ships.navy_ID = navy.navy_ID");
// $submissionsShipList = $shipList->fetchAll(); 


?>