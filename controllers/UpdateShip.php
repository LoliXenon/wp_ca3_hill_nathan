<?php 

return function($req, $res)
{
    $db = include_once('lib/database.php');

    $updateShip = $req->query('ship_ID');

    $shipList = $db->prepare("SELECT s.ship_ID, s.navy_ID, s.ship_name, s.ship_class, n.navy_acronym
    FROM ships s, navy n
    WHERE s.navy_id = n.navy_id
    AND s.ship_ID LIKE :ship_ID");
    $shipList->bindParam('ship_ID', $updateShip);
    $shipList->execute();
    $result = $shipList-> fetch();

    $res->render('main', 'update-ship', [
        'array' => $result,
        'pageTitle' => 'Update Ship',
        'success' => $req->query('success') === '1'
    ]);
};


?>