<?php 

return function($req, $res)
{
    $db = include_once('lib/database.php');

    $updateNavy = $req->query('navy_ID');

    $navyList = $db->prepare("SELECT navy_ID, navy_name, navy_acronym
    FROM navy
    WHERE navy_ID = :navy_ID");
    $navyList->bindParam('navy_ID', $updateNavy);
    $navyList->execute();
    $result = $navyList-> fetch();

    $res->render('main', 'update-navy', [
        'array' => $result,
        'pageTitle' => 'Update Navy'
    ]);
};


?>