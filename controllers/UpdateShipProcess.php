<?php

return function($req, $res)
{

    $shipName = $req->body('ship_name');
    $shipClass = $req->body('ship_class');
    $shipID = $req->query('ship_ID');

    try
    {

        $db = include_once('lib/database.php');

        $prepared = "UPDATE ships 
        SET ship_name = :ship_Name,
        ship_class = :ship_Class
        WHERE ship_ID = :ship_ID";
        $updateShip = $db->prepare($prepared);
        $updateShip->bindParam('ship_Name', $shipName, PDO::PARAM_STR);
        $updateShip->bindParam('ship_Class', $shipClass, PDO::PARAM_STR);
        $updateShip->bindParam('ship_ID', $shipID, PDO::PARAM_STR);
        $updateShip->execute();

        $res->redirect('/ship_list?success=1');

    }
    catch (PDOException $e)
    {
        $res->redirect('/ship_list?success=0');
    }
    catch (Exception $e)
    {
        echo("fucks");
    }
}

?>