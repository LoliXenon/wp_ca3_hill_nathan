<?php

return function($req, $res)
{
    

    $db = include_once('lib/database.php');

    $navyList = $db->prepare("SELECT navy_ID, navy_name, navy_acronym
    FROM navy
    ORDER BY navy_ID");
    $navyList->execute();
    $result = $navyList-> fetchAll();

    $res->render('main', 'navy-view', [
        'array' => $result,
        'pageTitle' => 'Navy List'
    ]);
};

?>