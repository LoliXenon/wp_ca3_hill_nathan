<?php

return function($req, $res)
{
    $db = include_once('lib/database.php');

    $prepared = "SELECT navy_name, navy_ID FROM navy";
    $navyList = $db->prepare($prepared);
    $navyList->execute();
    $result = $navyList->fetchAll();

    $res->render('main', 'remove-navy', [
        'array' => $result
    ]);
}


?>