<?php

return function($req, $res)
{

    $navyName = $req->body('navy_name');
    $navyAcronym = $req->body('navy_acronym');
    $navyID = $req->query('navy_ID');

    try
    {

        $db = include_once('lib/database.php');

        $prepared = "UPDATE navy 
        SET navy_name = :navy_Name, 
        navy_acronym = :navy_Acronym
        WHERE navy_ID = :navy_ID";
        $updateNavy = $db->prepare($prepared);
        $updateNavy->bindParam('navy_Name', $navyName, PDO::PARAM_STR);
        $updateNavy->bindParam('navy_Acronym', $navyAcronym, PDO::PARAM_STR);
        $updateNavy->bindParam('navy_ID', $navyID, PDO::PARAM_STR);
        $updateNavy->execute();

        $res->redirect('/navy_list?success=1');

    }
    catch (PDOException $e)
    {
        $res->redirect('/navy_list?success=0');
    }
    catch (Exception $e)
    {
        echo("fucks");
    }
}

?>