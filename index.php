<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

try
{

  $config = include('config.php');
  

  define('APP_BASE_PATH', '/wp_ca3_Hill_Nathan');

  // Include the Rapid library
  require_once('lib/Rapid.php');

  
  //$database = include_once('lib/database.php');

  // Create a new Router instance
  $app = new \Rapid\Router();

  // Define some routes. Here: requests to / will be
  // processed by the controller at controllers/Home.php
  //GET Routes
  $app->GET('/',        'Home');
  $app->GET('/ship_list', 'ShipList');
  $app->GET('/navy_list', 'NavyList');
  $app->GET('/add_ship', 'AddShip');
  $app->GET('/add_navy', 'AddNavy');
  $app->GET('/remove_ship', 'RemoveShip');
  $app->GET('/remove_navy', 'RemoveNavy');
  $app->GET('/update_ship', 'UpdateShip');
  $app->GET('/update_navy', 'UpdateNavy');

  //POST Routes
  $app->POST('/add_ship', 'AddShipProcess');
  $app->POST('/add_navy', 'AddNavyProcess');
  $app->POST('/update_ship', 'UpdateShipProcess');
  $app->POST('/update_navy', 'UpdateNavyProcess');
  $app->POST('/remove_ship', 'RemoveShipProcess');
  $app->POST('/remove_navy', 'RemoveNavyProcess');


  // Process the request
  $app->dispatch();

}
catch(\Rapid\RouteNotFoundException $e)
{
  $res = $e->getResponseObject();
  $res->render('main', '404', []);
}
catch(PDOException $e)
{
  echo("Send help");
}
catch(Exception $e)
{
  http_response_code(500);
  exit();
} 



?>