// When the document is fully loaded...
$('document').ready(() => {

  /**
   * #######################################################
   * # External link interception (confirm navigation)
   * #######################################################
   */
  $('a.external').click((event) => {

    event.preventDefault();

    const $clicked   = $(event.target).closest('a.external');
    const href       = $clicked.attr('href');
    const keep_going = confirm(`Are you sure you want to visit: ${href}`);

    if (keep_going) {
      window.location.href = href;
    }

  });


  /**
   * #######################################################
   * # Remove list items on double click (the dynamic way)
   * #######################################################
   */
//  $('body').on('dblclick', 'main li', (event) => {
//
//    const $clicked = $(event.target).closest('main li');
//    $clicked.remove();
//
//  });


  /**
   * #######################################################
   * # Add new list items (form input)
   * #######################################################
   */

   $('#AddListItemForm').submit((event) => {

    event.preventDefault();
    const $input = $('#AddListItemForm input[type="text"]');
    const value  = $input.val();

    if (value) {
      $('main ul').append(`<li>${value}</li>`);
      $input.val('');
    }

   });


  /**
   * #######################################################
   * # Tabs
   * #######################################################
   */

   $('a.tab-trigger').click((event) => {

    event.preventDefault();

    const $clicked     = $(event.target).closest('a.tab-trigger');
    const id_to_open   = $clicked.attr('href');
    const $tab_to_open = $(id_to_open);

    $clicked.addClass('active');
    $clicked.siblings('a').removeClass('active');

    $tab_to_open.addClass('active');
    $tab_to_open.siblings('div').removeClass('active');

   });
   


});