# Ship Site
This is a site that I designed for recording a country's navy, and then recording the ships that they have.
Most modern navies have a 3 character acronym preceding their ship names, which this database reflects.

## Features
This site has the following features:
* A MVC framework to make the website code more resusable
* A one to many table system consisting of the navy and ships tables
* View both navies and their ships
* Add a navy or a ship
* Delete a navy or a ship
* Update a navy or a ship

### Viewing Ships and Navies
The **View** files for both the Navy and Ships table are accessed via the header. 

### Adding a Navy
The **Insert** page is accessed from the Navy List page.
To add a navy, you need the name and the 3 letter acronym assigned to it.
>The RegEx currently accepts blank values, though this is erroneous. It properly rejects special characters.

### Adding a Ship
The **Insert** page is accessed from the Ship List page.
To add a ship, you need the name of the ship and the class it belongs to.
A drop down will be presented that allows you to choose the fleet the ship belongs to, which then provides the appropriate navy ID in the controller.
>The RegEx currently accepts blank values, though this is erroneous. It properly rejects special characers.

### Updating a Ship/Navy
The **Update** pages for the navy and ship tables are similar to the insert pages.
The update page is accessed from a link adjacent to the ship/navy lists.
>The same RegEx used in the inserts is applied here.

### Deleting a ship/Navy
The **Delete** page is access from a button on the appropriate page.
The page presents a drop-down menu from the values stored in the tables, and they will be deleted when the delete navy button is selected.
>JS was prepared to offer a confirmation menu, but not finished on time.



## To Do
* Fixing the erroneous options in the RegEx, as well as offering Server side validation
* Offer a function to sort ships by a column other than their navy ID, whih currently just groups the ships by their navy
* Add JS functions to add confirmation requests to the delete and update functions
* Add an option to upload a picture of the ship that you are adding to the database

## Credits
|Contributor | Area
--- | ---
Cameron Scholes | Assistance with Insert issues, adjustments to config and database files
James Farrell | Assistance with Update issues, assistance with Basic RegEx validation